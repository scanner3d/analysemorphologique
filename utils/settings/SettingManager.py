"""
Created on Fri Apr 21 2023
@name:   SettingManager.py
@desc:   A module to manage the settings
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class SettingManager:
    """
    A class to manage the settings

    :ivar settings: The settings
    :ivar instance: The instance of the class
    """
    instance = None
    version = "1.2.3"
    default_settings = {
                'name':'Analyse Morphologique',
                'authors':['Djalim Simaila <djalim.simaila@inrae.fr>','Alexis Doghmane <alexis@doghmane.fr>'],
                'description':"""Analyse Morphologique est un outil conçu pour extraire des informations \ndétaillées et précises à partir d'un scan 3D. Il permet aux utilisateurs\nd'analyser des objets 3D à partir d'un point de vue morphologique,\nc'est-à-dire en analysant la forme et la structure de l'objet.""",
                'repo':'https://forgemia.inra.fr/scanner3d/analysemorphologique',
                'lastGraph':["Aucun" for i in range(11)],
                'discretisation_method':'Z0-Zi >= DeltaZ', # 'Z0-Zi < DeltaZ'
                'discretisation_methods': ['Z0-Zi >= DeltaZ', 'Z0-Zi < DeltaZ'],
                'raw_data_suffix':'_delta_{delta_z}_analyse_brute',
                'discrete_data_suffix':'_delta_{delta_z}_analyse_rayon',
                'output_file_extension':'.txt',
                'output_file_separator':'\t',
                'pretiffy_output_file':True,
                "should_verticalise":True,
                "add_headers": True,
                "2D_renderer" : "matplotlib",
                "2D_renderers" : ["vispy","matplotlib"]
    }

    def __init__(self):
        try:
            with open('config.yml', 'r') as f:
                self.settings = load(f.read(), Loader=Loader)
            if self.settings['version'] != self.version:
                self.settings['version'] = self.version
                self.save()
            # if new settings are added, add them to the config file
            for key,value in self.default_settings.items():
                if key not in self.settings:
                    self.settings[key] = value
                    self.save()
        except FileNotFoundError:
            self.settings = {}
            self.createInitialSettings()
        self.has_changed = False

    @staticmethod
    def get_instance()->'SettingManager':
        """
        Get the instance of the class

        :return: The instance of the class
        """
        if SettingManager.instance is None:
            SettingManager.instance = SettingManager()
        return SettingManager.instance


    def save(self):
        """
        Save the settings to the config file
        """
        self.has_changed = True
        with open('config.yml', 'w') as f:
            f.write(dump(self.settings, Dumper=Dumper))

    def remove_changed(self):
        """
        Remove the changed flag
        """
        self.has_changed = False

    def createInitialSettings(self):
        self.settings = self.default_settings
        self.settings['version'] = self.version
        self.save()

    def get_last_graph(self, graph_number)->str:
        """
        Get the last graph name for the specified slot
        """
        return self.settings['lastGraph'][graph_number]

    def set_last_graph(self, graph_number, graph_name):
        """
        Set the last graph name for the specified slot
        
        :param graph_number: The slot number
        :param graph_name: The graph name
        """
        self.settings['lastGraph'][graph_number] = graph_name
        self.save()

    def get_setting(self, setting_name)->any:
        """
        Get the value of the specified setting

        :param setting_name: The name of the setting
        :return: The value of the setting
        """
        return self.settings[setting_name]
    
    def get_settings(self)->dict:
        """
        Get the settings

        :return: The settings
        """
        return self.settings
    
    def set_setting(self, setting_name, setting_value):
        """
        Set the value of the specified setting

        :param setting_name: The name of the setting
        :param setting_value: The value of the setting
        """
        self.settings[setting_name] = setting_value
        self.save()