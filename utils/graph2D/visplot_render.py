"""
Created on Fri Apr 21 2023
@name:   visplot_render.py
@desc:   A module to render a 2D data using vispy
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
import vispy.plot as vp
import numpy as np

def render2D(values:list,title:str,xlabel="",ylabel="",show:bool=True)->vp.Fig|None:
    """
    Render a 2D plot using vispy

    :param values: A list with the values
    :param title: Title of the plot
    :param xlabel: Label of the x axis
    :param ylabel: Label of the y axis
    :param show: If True, show the plot in its own window, else return the canvas
    """
    fig = vp.Fig(size=(600, 500), show=False)
    plotwidget = fig[0, 0]
    fig.title = title
    plotwidget.plot(values,
                    marker_size=0,
                    width=2,
                    title=title,
                    xlabel=xlabel,
                    ylabel=ylabel)
    if show:
        fig.show(run=True)
    else:
        return fig.native

def cross_section(x_values:list, y_values:list,title:str,xlabel="",ylabel="",show:bool=True)->vp.Fig|None:
    """
    Render a 2D cross section using vispy

    :param x: A list with the x values
    :param y: A list with the y values
    :param title: Title of the plot
    :param xlabel: Label of the x axis
    :param ylabel: Label of the y axis
    :param show: If True, show the plot in its own window, else return the canvas
    """
    color = (0.3, 0.5, 0.8,.8)
    fig = vp.Fig(show=False)
    line = fig[0:4, 0:4].plot(np.column_stack((x_values,y_values)),
                            symbol='disc',
                            width=0,
                            face_color=color,
                            edge_color=None,
                            marker_size=8,
                            title=title,
                            xlabel=xlabel,
                            ylabel=ylabel)
    line.set_gl_state(depth_test=False)
    if show:
        fig.show(run=True)
    else:
        return fig.native