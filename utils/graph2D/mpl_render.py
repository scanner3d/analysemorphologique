"""
Created on Fri Apr 21 2023
@name:   mpl_render.py
@desc:   A module to render a 2D data using matplotlib, thoses functions cant be integrated into pyqt gui yet
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg,NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from PyQt5.QtWidgets import QWidget, QVBoxLayout



class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

def render2D(values:list,title:str,xlabel="",ylabel="",show:bool=True):
    """
    Render a 2D model using matplotlib

    :param values: A list with the values
    """
    sc = MplCanvas(None, width=5, height=4, dpi=100)
    x = [i[0] for i in values]
    y = [i[1] for i in values]
    sc.axes.plot(x,y)
    sc.axes.set_title(title)
    sc.axes.set_xlabel(xlabel)
    sc.axes.set_ylabel(ylabel)
    toolbar = NavigationToolbar(sc, None)
    widget = QWidget()
    layout = QVBoxLayout()
    layout.addWidget(sc)
    layout.addWidget(toolbar)
    widget.setLayout(layout)
    if show:
        plt.show()
    else:
        return widget

def cross_section(x_values:list, y_values:list,title:str,xlabel="",ylabel="",show=True):
    """
    Render a 2D cross section using matplotlib
    
    :param x: A list with the x values
    :param y: A list with the y values
    """
    sc = MplCanvas(None, width=5, height=4, dpi=100)
    sc.axes.scatter(x_values,y_values)
    sc.axes.set_title(title)
    sc.axes.set_xlabel(xlabel)
    sc.axes.set_ylabel(ylabel)
    toolbar = NavigationToolbar(sc, None)
    widget = QWidget()
    layout = QVBoxLayout()
    layout.addWidget(sc)
    layout.addWidget(toolbar)
    widget.setLayout(layout)
    if show:
        plt.show()
    else:
        return widget