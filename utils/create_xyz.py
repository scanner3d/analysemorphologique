import random
import math

def create_circle(radius:float, center_x:float, center_y:float,points_number:int=100):
    """
    Create a circle with the given radius and center
    :param radius: Radius of the circle
    :param center_x: X coordinate of the center
    :param center_y: Y coordinate of the center
    :param points_number: Number of points to create the circle
    :return: list of points
    """
    cpt = 0
    angles = []
    while cpt < 2*math.pi:
        angles.append(cpt)
        cpt += 2*math.pi/points_number
    return [[round(radius * math.cos(angle) + center_x,6), round(radius * math.sin(angle) + center_y,6)] for angle in angles]

def create_cylinder(radius:float, center_x:float, center_y:float, height:float, points_number:int=100):
    """
    Create a cylinder with the given radius, center and height
    :param radius: Radius of the cylinder
    :param center_x: X coordinate of the center
    :param center_y: Y coordinate of the center
    :param height: Height of the cylinder
    :param points_number: Number of points to create the circle
    :return: list of points
    """
    circle = create_circle(radius,center_x,center_y,points_number)
    cylinder = []
    for z in range(int(height)):
        cylinder.append([[x,y,z] for x,y in circle])
    return cylinder

if __name__ == "__main__":
    with open("test_cylinder.xyz",'w') as f:
        for couches in create_cylinder(10,0,0,10):
            for points in couches:
                x,y,z = points[0],points[1],points[2]
                f.write(f"{x} {y} {z}\n")
