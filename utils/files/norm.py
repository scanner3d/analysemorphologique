"""
Created on Fri Apr 14 2023
@name:   input.py
@desc:   x,y,z input cleaning (Deprecated)
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""

def denormalizeXYZ(filePath:str, output:str):
    """
    Denormalize an XYZ file
    :param filePath: Path to the XYZ file
    :param output: Path to the output file
    """
    with open(filePath, 'r') as f:
        data = f.readlines()
        x = [float(line.split()[0]) for line in data]
        y = [float(line.split()[1]) for line in data]
        z = [float(line.split()[2]) for line in data]
        with open(output, 'w') as f:
            for i in range(len(x)):
                f.write(f'{x[i]} {y[i]} {z[i]}\n')