"""
Created on Mon Apr 17 2023
@name:   output.py
@desc:   Output file formatting and saving
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
import datetime
from utils.settings.SettingManager import SettingManager

def generate_headers(filename:str,delta_z:float) -> str:
    """
    """
    headers = f"""##############################
Analyse Morphologique 
version :{SettingManager.get_instance().get_setting("version")}

filename : {filename}
date : {datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")}
discretisation : {SettingManager.get_instance().get_setting("discretisation_method")}
delta_z : {delta_z}
had been veticalised : {"yes" if SettingManager.get_instance().get_setting("should_verticalise") else "no"}
##############################
"""
    return headers


def format_data(data:dict, separator:str, selected_columns:list = None) -> str:
    """
    Format the data to be saved in the output file.

    :param data: Data to be formatted
    :param selected_columns: Columns to be saved (in the specified order)
    :param separator: Separator of the columns
    :return: Formatted data

    Example:
        >>> data = {
        ...     'col1': [1, 2, 3],
        ...     'col2': [4, 5, 6],
        ...     'col3': [7, 8, 9]
        ... }
        >>> format_data(data, separator=';')
        'col1;col2;col3
         1   ;4   ;7
         2   ;5   ;8
         3   ;6   ;9
        '
    """
    
    # Get the setting for the output file
    is_prettier = SettingManager.get_instance().get_setting('pretiffy_output_file')
    output = ''
    # If the columns are not specified, we take all the columns from dict
    if selected_columns is None:
        selected_columns = list(data.keys())
    
    # Write the columns headers
    for column_name in selected_columns:
        if is_prettier:
            output += column_name.ljust(len(column_name) if len(column_name) > 8 else 9 ) + separator
        else:
            output += column_name + separator
    output += '\n'
    # Write the columns values
    for i in range(len(data[selected_columns[0]])):
        for column in selected_columns:
            if is_prettier:
                output += str(data[column][i]).ljust(len(column) if len(column) > 8 else 9 ) + separator
            else:
                output += str(data[column][i]) + separator
        output += '\n'
    return output

def save_output_file(output_file:str, content:str):
    """
    Save the output file.

    :param output_file: Path to the output file
    :param content: Content of the output file
    """
    with open(output_file, 'w',encoding='utf-8') as file:
        file.write(content)
