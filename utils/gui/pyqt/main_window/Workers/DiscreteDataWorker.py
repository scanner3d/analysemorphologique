"""
Created on Wed Apr 26 2023
@name:   DiscreteDataWorker.py
@desc:   A module to process discrete data in a thread
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
from PyQt5.QtCore import pyqtSignal
from utils.files.input import ScannedObject
from utils.files.output import save_output_file, format_data, generate_headers
from utils.gui.pyqt.main_window.Workers.Worker import Worker
from utils.data_processing.data_processing import get_discrete_data
from utils.settings.SettingManager import SettingManager

class DiscreteDataProcessWorker(Worker):
    """
    Worker to calculate the discrete data in a thread

    :param name: The name of the worker
    :param obj: The scanned object
    :param output_path: The path to save the output file
    :param output_file_prefix: The prefix of the output file
    :param delta_z: The delta z

    :ivar obj: The scanned object
    :ivar delta_z: The delta z
    :ivar output_path: The path to save the output file
    :ivar output_file_prefix: The prefix of the output file
    :ivar processedData: The signal to emit the result
    """
    processedData = pyqtSignal(dict)

    def __init__(self,name:str, obj:ScannedObject,output_path:str,output_file_prefix:str,delta_z:float):
        super().__init__(name)
        self.obj = obj
        self.delta_z = delta_z
        self.output_path = output_path
        self.output_file_prefix = output_file_prefix


    def run(self):
        """
        This function is called when the thread is started.
        It calculates the discrete data and emits a dict with the following keys:
            - X moy (en mm)
            - Y moy (en mm)
            - Z moy (en mm)
            - Discretisation(en mm)
            - Rayon moyen (en mm)
            - Rayon ecart type (en mm)
        It also saves the result in a file that'll be located in the output_path
        """
        self.set_status("Calculating discrete data...")
        # Execute the analyse
        discrete_data = get_discrete_data(self.obj, 6,self.delta_z,self.update_progress)
        # Emit the result
        self.processedData.emit(discrete_data)
        self.set_weight(100)
        self.set_status("Saving data...")
        self.update_progress(10)
        # Save the result
        suffix = SettingManager.get_instance().get_setting('discrete_data_suffix').replace('{delta_z}',str(self.delta_z))
        extension = SettingManager.get_instance().get_setting('output_file_extension')
        separator = SettingManager.get_instance().get_setting('output_file_separator')

        output = ""
        add_headers = SettingManager.get_instance().get_setting("add_headers")
        if add_headers:
            output += generate_headers(self.obj.filename,self.delta_z)
        output += format_data(discrete_data,
                            separator,
                            ["X moy (en mm)",
                            "Y moy (en mm)",
                            "Z moy (en mm)",
                            "Discretisation(en mm)",
                            "Rayon moyen (en mm)",
                            "Rayon ecart type (en mm)"])
        save_output_file(f'{self.output_path}/{self.output_file_prefix}{suffix}{extension}', output)
        self.set_status("Done !")
        self.finished.emit()