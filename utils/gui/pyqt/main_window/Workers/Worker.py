"""
Created on Wed Apr 26 2023
@name:   Worker.py
@desc:   Base class for the workers
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
from PyQt5.QtCore import pyqtSignal, QObject

class Worker(QObject):
    """
    Base class for the workers

    :param name: The name of the worker

    :ivar name: The name of the worker
    :ivar progress_value: The current progress value
    :ivar progress_weight: The weight of the progress bar
    :ivar finished: The signal to emit when the worker is finished
    :ivar progress: The signal to emit the progress value
    :ivar status: The signal to emit the status of the worker
    """
    finished = pyqtSignal()
    progress = pyqtSignal(int)
    status = pyqtSignal(str)

    def __init__(self,name:str):
        super().__init__()
        self.name = name
        self.progress_value = 0
        self.progress_weight = 100

    def set_status(self, status:str):
        """
        Set the weight of the progress bar
        """
        self.status.emit(f"[{self.name}]: {status}")

    def set_weight(self, weight):
        """
        Set the weight of the progress bar
        """
        self.progress_weight = weight

    def update_progress(self, percent):
        """
        Update the progress bar
        """
        self.progress_value += int(percent/100*self.progress_weight)
        self.progress.emit(self.progress_value)