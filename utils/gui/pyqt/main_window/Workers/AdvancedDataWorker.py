"""
Created on Thu May 4 2023
@name:   AdvancedDataWorker.py
@desc:   A module to process discrete data and extract morphological indicators
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
from PyQt5.QtCore import pyqtSignal
from utils.gui.pyqt.main_window.Workers.Worker import Worker
from utils.data_processing.data_processing import get_advanced_data

class AdvancedDataWorker(Worker):
    """
    Worker to calculate the discrete data in a thread

    :param name: The name of the worker
    :param discrete_data: The discrete data to process
    :param raaw_data: The raw data to process

    :ivar processedData: Signal emitted when the data is processed
    :ivar discrete_data: The discrete data to process
    :ivar raw_data: the raw data to process
    """
    processedData = pyqtSignal(dict)

    def __init__(self,name:str, discrete_data:dict,raw_data:dict, V_scan:float):
        super().__init__(name)
        self.discrete_data = discrete_data
        self.raw_data = raw_data
        self.V_scan = V_scan


    def run(self):
        """
        This function is called when the thread is started.
        It calculates the advanced data and emits a dict with the following keys:
            - Tortuosite
            - Volume en mm3
            - Surface en mm2
            - Moyenne des rayons moyens
            - Ecart-type des rayons moyens
            - Sigma r tot
            - MI_l
            - MI_p
        """
        self.set_status("Calculating advanced data...")
        advanced_data = get_advanced_data(self.discrete_data,self.raw_data, self.V_scan, self.update_progress)
        self.processedData.emit(advanced_data)
        self.set_status("Done")
        self.finished.emit()