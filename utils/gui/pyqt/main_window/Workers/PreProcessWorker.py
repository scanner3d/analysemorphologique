"""
Created on Wed Apr 26 2023
@name:   PreProcessWorker.py
@desc:   A module to pre process the 3d object in a thread
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
import os
from utils.gui.pyqt.main_window.Workers.Worker import Worker
from PyQt5.QtCore import pyqtSignal
from utils.files.input import ScannedObject
from utils.math.position_manipulation import verticalise
from utils.settings.SettingManager import SettingManager

class PreProcessWorker(Worker):
    """
    Worker to pre process the 3d object in a thread

    :param name: The name of the worker
    :param objpath: The path to the 3d object
    :param discretisation_value: The delta z

    :ivar objpath: The path to the 3d object
    :ivar deltaZ: The delta z
    :ivar progress_value: The current progress value
    :ivar progress_weight: The weight of the progress bar
    :ivar processed_obj: The signal to emit the processed object

    :method run: Pre process the object, read the file, verticalise it and normalise it
    """
    processed_obj = pyqtSignal(ScannedObject)

    def __init__(self,name:str, objpath:str,discretisation_value:float):
        super().__init__(name)
        self.objpath = objpath
        self.deltaZ = discretisation_value
        self.progress_value = 0
        self.progress_weight = 100

    def run(self):
        """
        This function is called when the thread is started.
        It reads the file specified in the objpath, parses it, verticalises it and normalises it.
        It then emits the processed object.
        """
        # Read the file
        self.set_status("Loading file...")
        obj = ScannedObject.from_file(self.objpath)
        obj.set_filename(os.path.basename(self.objpath))
        self.update_progress(5)

        # Verticalise the object
        if SettingManager.get_instance().get_setting("should_verticalise"):
            self.set_status("Verticalising object...")
            verticalise(obj)
        self.update_progress(5)

        # Normalise the object
        self.set_status("Normalising object...")
        obj.normalise()
        self.update_progress(5)

        self.set_status("Discretising object...")
        obj.get_discrete_vertices(self.deltaZ)
        self.update_progress(5)

        # Emit the processed object
        self.processed_obj.emit(obj)
        self.finished.emit()
