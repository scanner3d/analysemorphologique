import utils.gui.pyqt.main_window.Workers.Worker as Worker
import utils.gui.pyqt.main_window.Workers.AdvancedDataWorker as AdvancedDataWorker
import utils.gui.pyqt.main_window.Workers.DiscreteDataWorker as DiscreteDataWorker
import utils.gui.pyqt.main_window.Workers.PreProcessWorker as PreProcessWorker
import utils.gui.pyqt.main_window.Workers.RawDataWorker as RawDataWorker    