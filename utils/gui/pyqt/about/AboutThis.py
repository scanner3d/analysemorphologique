"""
Created on Tue May 02 2023
@name:   AboutThis.py
@desc:   A class to show a popup with the about this
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
from PyQt5 import QtCore, QtWidgets
from utils.gui.pyqt.about.UI_AboutThis import Ui_AboutThis
from utils.settings.SettingManager import SettingManager

class AboutThis(QtWidgets.QMainWindow,Ui_AboutThis):
    """
    """
    def __init__(self, parent=None) -> None:
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        settings = SettingManager.get_instance()
        self.Description.setText(settings.get_setting('description'))
        for author in settings.get_setting('authors'):
            author_label = QtWidgets.QLabel()
            author_label.setText(author)
            self.authors_list.addWidget(author_label)
        self.version_litteral.setText(settings.get_setting('version'))