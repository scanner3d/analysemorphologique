"""
Created on Thu Apr 27 2023
@name:   Settings.py
@desc:   Settings window
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
import os
from PyQt5 import QtWidgets
from utils.gui.pyqt.settings.UI_Settings import Ui_Settings
from utils.settings.SettingManager import SettingManager

class Settings(QtWidgets.QMainWindow,Ui_Settings):
    """
    Settings window
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        # Retrieve the UI
        self.setupUi(self)
        # Setup buttons listeners
        settings = SettingManager.get_instance()
        self.discretisation_method.addItems(settings.get_setting('discretisation_methods'))
        self.discretisation_method.setCurrentText(settings.get_setting('discretisation_method'))
        self.raw_data_suffix.setText(settings.get_setting('raw_data_suffix'))
        self.discrete_data_suffix.setText(settings.get_setting('discrete_data_suffix'))
        self.extention.setText(settings.get_setting('output_file_extension'))
        self.separator.setText(settings.get_setting('output_file_separator'))
        self.prettify.setChecked(settings.get_setting('pretiffy_output_file'))
        self.should_verticalise.setChecked(settings.get_setting('should_verticalise'))
        self.add_headers.setChecked(settings.get_setting("add_headers"))


        #Connect to onchanged actions
        self.discretisation_method.currentTextChanged.connect(self.accept)
        self.raw_data_suffix.textChanged.connect(self.accept)
        self.discrete_data_suffix.textChanged.connect(self.accept)
        self.extention.textChanged.connect(self.accept)
        self.separator.textChanged.connect(self.accept)
        self.prettify.stateChanged.connect(self.accept)
        self.should_verticalise.stateChanged.connect(self.accept)
        self.add_headers.stateChanged.connect(self.accept)


    def accept(self):
        """
        Accept the changes
        """
        settings = SettingManager.get_instance()
        settings.set_setting('discretisation_method', self.discretisation_method.currentText())
        settings.set_setting('raw_data_suffix', self.raw_data_suffix.text())
        settings.set_setting('discrete_data_suffix', self.discrete_data_suffix.text())
        settings.set_setting('output_file_extension', self.extention.text())
        if self.separator.text() == '\\t':
            settings.set_setting('output_file_separator', '\t')
        else:
            settings.set_setting('output_file_separator', self.separator.text())
        settings.set_setting('pretiffy_output_file', self.prettify.isChecked())
        settings.set_setting('should_verticalise', self.should_verticalise.isChecked())
        settings.set_setting("add_headers", self.add_headers.isChecked())