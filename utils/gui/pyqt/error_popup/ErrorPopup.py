"""
Created on Wed Apr 28 2023
@name:   ErrorPopup.py
@desc:   A class to show a popup with an error message
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
from PyQt5.QtWidgets import QMessageBox

class ErrorPopup(object):
    """
    A class to show a popup with an error message

    :param error_text: The error message
    :param details: The details of the error
    :param button_label: The label of the button
    :param button_callback: The callback of the button

    :ivar error_text: The error message
    :ivar details: The details of the error
    :ivar button_label: The label of the button
    :ivar button_callback: The callback of the button

    :method show_popup: Show the popup
    """

    def __init__(self,error_text,details = None,button_label = None,button_callback=None):
        self.error_text = error_text
        self.button_label = button_label
        self.button_callback = button_callback
        self.details = details

    def show_popup(self):
        """
        Show the error popup with the specified error message.
        If a button label and a button callback are specified, the popup will
        have a button with the specified label and the specified callback.
        """
        msg = QMessageBox()
        msg.setWindowTitle("Erreur")
        msg.setText("Erreur: " + self.error_text)
        msg.setIcon(QMessageBox.Critical)

        if self.button_label is not None and self.button_callback is not None:
            msg.setStandardButtons(QMessageBox.Cancel|QMessageBox.Retry)
            msg.setDefaultButton(QMessageBox.Cancel)
            msg.button(QMessageBox.Cancel).clicked.connect(msg.close)
            msg.button(QMessageBox.Retry).setText(self.button_label)
            msg.button(QMessageBox.Retry).clicked.connect(self.button_callback)
        else:
            msg.setStandardButtons(QMessageBox.Ok)
            msg.setDefaultButton(QMessageBox.Ok)
            msg.button(QMessageBox.Ok).clicked.connect(msg.close)
        msg.setInformativeText(self.error_text)
        if self.details is not None:
            msg.setDetailedText(self.details)
        msg.exec_()