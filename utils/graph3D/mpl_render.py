"""
Created on Fri Apr 21 2023
@name:   mpl_render.py
@desc:   A module to render a 3D data using matplotlib, thoses functions cant be integrated into pyqt gui yet
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""

from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg,NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
from utils.files.input import ScannedObject
from PyQt5.QtWidgets import QWidget, QVBoxLayout

class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        self.fig = plt.figure(figsize=(width, height), dpi=dpi)
        super(MplCanvas, self).__init__(self.fig)


def render3D(obj:ScannedObject,show:bool=True):
    """
    Render a 3D model using matplotlib's Poly3dcollection
    
    :param obj: A ScannedObject to be rendered
    """
    sc = MplCanvas(None, width=5, height=4, dpi=100)
    sc.axes = sc.fig.add_subplot(projection='3d')
    faces = np.array(obj.get_faces())
    verts = np.array(obj.get_vertices())
    mesh = Poly3DCollection(verts[faces], alpha=0.25, edgecolor='none')
    sc.axes.add_collection3d(mesh)
    sc.axes.set_box_aspect((1, 1, 1), zoom=0.01)

    toolbar = NavigationToolbar(sc, None)
    widget = QWidget()
    layout = QVBoxLayout()
    layout.addWidget(sc)
    layout.addWidget(toolbar)
    widget.setLayout(layout)
    

    if show:
        plt.grid(False)
        plt.axis('off')
        plt.show()
    else:
        return widget