#!/usr/bin/env python3
"""
Created on Fri Apr 21 2023
@name:   app.py
@desc:   Main file for the application
@auth:   Djalim Simaila
@e-mail: djalim.simaila@inrae.fr
"""
import sys
from PyQt5.QtWidgets import QApplication
from utils.gui.pyqt.main_window.MainWindow import MainWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()