# Analyse Morphologique

"Analyse morphologique" est un outil développé par le laboratoire de géomécanique du centre PACA d'INRAE. Son objectif est d'extraire des informations sur la morphologie de scans 3D provenant de tests HET (Hydraulic Erosion Test).

## Fonctionnalités

L'outil dispose des fonctionnalités suivantes :

### Analyse de l'objet

- Lecture de fichiers .obj, .xyz et .stl
- Redressement et normalisation de l'objet
- Discrétisation de l'objet en couches de taille égale
- Calcul d'indices morphologiques
- Export des indices morphologiques dans un fichier

### Affichage de graphiques

L'outil permet également l'affichage de plusieurs graphiques, tels que :

- Le maillage de l'objet
- La coupe de l'objet sur les axes xz et yz
- L'évolution du rayon moyen en fonction de la hauteur
- L'évolution de la différence entre le rayon moyen de chaque couche et la moyenne des rayons moyens en fonction de la hauteur
- La coupe d'une couche sélectionnée
- L'évolution de la différence entre le rayon de chaque point et le rayon moyen de la couche en fonction de l'angle theta (de 0 à 2pi).

![images/1.png](images/1.png)
![images/2.png](images/2.png)
![images/3.png](images/3.png)

---

## Installation et utilisation

Veuillez noter que ce programme nécessite Python 3.10 ou supérieure.

### Installation automatisée

Le dépôt contient des scripts qui facilitent l'utilisation du programme. Cependant, ces scripts supposent que Python 3 est installé et accessible dans le PATH.

Utilisateurs Windows : Exécutez `install_deps.bat` pour installer les dépendances. Cette étape n'est nécessaire qu'une seule fois. Ensuite, exécutez `launch.bat` pour lancer le programme.

Utilisateurs Linux/Mac : Exécutez `install_deps.sh` pour installer les dépendances. Cette étape n'est nécessaire qu'une seule fois. Ensuite, exécutez `launch.sh` pour lancer le programme.

Utilisateurs Mac : Exécutez `install_deps.command` pour installer les dépendances. Cette étape n'est nécessaire qu'une seule fois. Ensuite, exécutez `launch.command` pour lancer le programme.

### Installation manuelle

Pour utiliser le programme, vous pouvez le cloner à partir du dépôt Git ou télécharger l'une des [versions disponibles](https://forgemia.inra.fr/scanner3d/analysemorphologique/-/releases) et installer les dépendances à l'aide de pip.

```bash
git clone https://forgemia.inra.fr/scanner3d/analysemorphologique.git
cd analysemorphologique
pip install -r requirements.txt
python main.py
```

### Problème de compatibilité avec la bibliothèque pyQt et utilisation de l'environnement virtuel

Si vous utilisez Anaconda, il est possible que vous rencontriez des problèmes avec la bibliothèque pyQt. Vous pourriez rencontrer une erreur telle que :

```
line 9, in <module>
from PyQt5.QtWidgets import QApplication
ImportError: DLL load failed while importing QtWidgets: La procédure spécifiée est introuvable.
```

Cependant, vous pouvez toujours utiliser le programme en créant un environnement virtuel avec les dépendances nécessaires. Pour cela, vous devez créer un environnement virtuel, l'activer, puis installer les dépendances.

```bash
pip install venv
python -m venv venv
# Pour les utilisateurs de windows
venv\Scripts\activate.bat
# Pour les utilisateurs de linux ou mac
source venv/bin/activate
pip install -r requirements.txt
python main.py
```

### Instructions pour l'utilisation des graphiques 2D et 3D

Graphiques 2D :

- Pour zoomer, utilisez la molette de la souris.
- Pour vous déplacer, cliquez sur le graphique et déplacez la souris.
- Pour modifier l'échelle, cliquez avec le bouton droit de la souris sur le graphique et déplacez la souris : 
  - vers le haut pour augmenter l'échelle de l'axe y.
  - vers le bas pour diminuer l'échelle de l'axe y.
  - vers la gauche pour diminuer l'échelle de l'axe x.
  - vers la droite pour augmenter l'échelle de l'axe x.

Graphiques 3D :

- Pour zoomer, utilisez la molette de la souris ou maintenez le clic droit et déplacez la souris vers le haut ou le bas.
- Pour vous déplacer, maintenez le bouton 'Majuscule', puis cliquez sur le graphique et déplacez la souris.
- Pour effectuer une rotation, maintenez le clic gauche et déplacez la souris : 
  - pour une rotation autour de l'axe x, déplacez la souris vers le haut ou le bas dans la zone indiquée en bleu.
  - pour une rotation autour de l'axe y, déplacez la souris dans une trajectoire de cercle suivant la zone indiquée en vert.
  - pour une rotation autour de l'axe z, déplacez la souris vers la gauche ou la droite dans la zone indiquée en rouge.

![images/4.png](images/4.png)

---

## Informations et Documentation pour les Developpeurs

### Informations

- Convention des variables : 
  - snake_case pour les fonctions et les variables.
  - PascalCase pour les classes.
- Les imports sont relatif au fichier exécuté. (ici main.py)
- L'interface graphique est modifiable avec [Qt Designer](https://build-system.fman.io/qt-designer-download). 
  - Le fichier .ui est ensuite converti en fichier .py avec la commande suivante : `pyuic5 -x <fichier.ui> -o <fichier.py>`
- Le code est commenté et documenté en anglais.
- Le backend du rendu de graphe "matplotlib" n'est (actuellement) pas compatible avec le programme. Il peut cepandant etre utilisé independamment du programme pour des scripts ou des notesbooks.

Stack trace de l'analyse d'un fichier .obj :

```mermaid
flowchart TD
    A(app.py)-- calls --> B(MainWindow.py)
    B -- event loop--> B
    B -- calls in a thread --> C(PreProcessWorker.py)
    C -- When finished, triggers and calls in a thread --> D(RawDataWorker.py)
    C -- When finished, triggers and calls in a thread --> E(DiscreteDataWorker.py)
    D -- Stores results in --> B
    D --> G(Save result in a file)
    E -- Stores results in --> B
    E --> G
    E -- When finished triggers and calls in a thread --> F(AdvancedDataWorker.py)
    F -- Stores results in --> B
```

La documentation du code est dans le dossier builded doc, il suffit d'ouvrir le fichier index.html avec un navigateur web.

Pour generer la documentation apres modification du code il faut passer par [sphinx](https://www.sphinx-doc.org/en/master/usage/installation.html),
ensuite il faut installer le theme [readthedocs](https://sphinx-rtd-theme.readthedocs.io/en/stable/)

```bash
pip install sphinx_rtd_theme
```

et executer la commande suivante dans le dossier doc :

```bash
make html
```

Cela generera la documentation dans le dossier docs/build.

---

## Contributeurs

- Djalim SIMAILA : [mail](mailto:DjalimS.Pro@outlook.fr), [github](https://github.com/DjalimSimaila)
  - Implementation de la specification du projet.
- Alexis DOGHMANE : [mail](mailto:alexis.doghmane@inrae.fr)
  - Redacteur des specifications du projet.

- Pierre PHILIPPE : [mail](mailto:pierre.philippe@inrae.fr)
  - Recherche et documentation sur les calculs d'analyse morphologique.

Avec du code de :

- Jerome Duriez :
  - Redressement de l'objet 3D en passant par un alignement des axes de l'espace avec les axes d'inertie de l'objet 3D.

- Rick van Hattem : [mail](mailto:Wolph@Wol.ph)
  - Code permettant de calculer le centre de gravité d'un objet, la matrice d'inertie et le volume d'un objet 3D. [source](https://pypi.org/project/numpy-stl/)
