utils.files.input.ResultFileNotGiven
====================================

.. currentmodule:: utils.files.input

.. autoexception:: ResultFileNotGiven