utils.gui.pyqt.main\_window.Workers.Worker.Worker
=================================================

.. currentmodule:: utils.gui.pyqt.main_window.Workers.Worker

.. autoclass:: Worker
   :members:
   :show-inheritance:
   :special-members: __call__, __add__, __mul__

   
   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~Worker.blockSignals
      ~Worker.childEvent
      ~Worker.children
      ~Worker.connectNotify
      ~Worker.customEvent
      ~Worker.deleteLater
      ~Worker.disconnect
      ~Worker.disconnectNotify
      ~Worker.dumpObjectInfo
      ~Worker.dumpObjectTree
      ~Worker.dynamicPropertyNames
      ~Worker.event
      ~Worker.eventFilter
      ~Worker.findChild
      ~Worker.findChildren
      ~Worker.inherits
      ~Worker.installEventFilter
      ~Worker.isSignalConnected
      ~Worker.isWidgetType
      ~Worker.isWindowType
      ~Worker.killTimer
      ~Worker.metaObject
      ~Worker.moveToThread
      ~Worker.objectName
      ~Worker.parent
      ~Worker.property
      ~Worker.pyqtConfigure
      ~Worker.receivers
      ~Worker.removeEventFilter
      ~Worker.sender
      ~Worker.senderSignalIndex
      ~Worker.setObjectName
      ~Worker.setParent
      ~Worker.setProperty
      ~Worker.set_status
      ~Worker.set_weight
      ~Worker.signalsBlocked
      ~Worker.startTimer
      ~Worker.thread
      ~Worker.timerEvent
      ~Worker.tr
      ~Worker.update_progress
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Worker.destroyed
      ~Worker.finished
      ~Worker.objectNameChanged
      ~Worker.progress
      ~Worker.staticMetaObject
      ~Worker.status
   
   