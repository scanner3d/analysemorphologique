.. Analyse Morphologique documentation master file, created by
   sphinx-quickstart on Tue May 16 10:01:13 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Analyse Morphologique's documentation!
=================================================

"Analyse Morphologique" (Morphological Analysis) is a tool developed by the Geomechanics Laboratory of INRAE PACA Center. Its goal is to extract information about the morphology of 3D scans from HET (Hydraulic Erosion Test) tests.
Check out the :doc:`installation` section for further information.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Contents
--------

.. toctree::

   installation
   usage
   api