Installation
============

Python Version
--------------
Analyse Morphologique is compatible with Python 3.10 and above.


Install Analyse Morphologique
-----------------------------

To use Analyse Morphologique, first get a release tarball or clone the repository from `here https://forgemia.inra.fr/scanner3d/analysemorphologique`_.
Then, install the dependencies with pip:

.. code-block:: console
   
   $ pip install -r requirements.txt

Once the dependencies installed, you can run Analyse Morphologique with

.. code-block:: console

   $ python main.py

.. note::
   If you have issues with the pyQt5 module, you can run Analyse Morphologique in a virtual envirenment.
   To do so, run the following commands:

   .. code-block:: console

      $ python -m venv .venv
      $ source .venv/bin/activate # on Windows, use .venv\Scripts\activate.bat instead
      (.venv) $ pip install -r requirements.txt
      (.venv) $ python main.py