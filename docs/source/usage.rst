Usage
=====

Graphical User Interface
------------------------

Analysis
^^^^^^^^

The GUI is started by running the **main.py** script. It requires the **PyQt5** package to be installed.
Please go to the :doc:`installation` section for more information on how to get the program working.
The GUI is divided into two main sections: the left side is used to select the input file, the output
directory, the different parameters, and to show the results when the analysis is done. The right side
is used to select the desired graphs the user wishes to show. The user can toggle the graphs panel on and off
with the **Afficher les graphiques** checkbox. 

.. image:: _images/main_window.png
   :width: 600

The input file can be selected by clicking the **Choisir un fichier** button or the **Ouvrir un fichier** menu item in the file
menu. The program can read .xyz, .obj, and .stl (both ASCII and binary) files. The file extension must be one of these three, or the program will
refuse to open it.

.. note::
   .xyz files do not contain any information on the face data, which is used to verticalize the scanned object. Therefore, the .xyz file
   must have been manually verticalized before being used with this program.

By default, the output folder is the folder where the file is located, but the user can change it by pressing **Choisir le répertoire**.
The program will create two files in the output folder: one containing the raw data of the file with some data associated with each point, and
one containing the discretized data. These files are prefixed with the name of the input file and suffixed with the values set in the program's `Settings`_.

The user can then choose how they want to discretize the scanned object by entering the desired interval size in the **Discrétisation en (mm)** field.
The default value is 1mm.

The user can now run the analysis by clicking on the **Analyser le fichier** button. If any parameters are missing, the program will display an error message
prior to running the analysis, prompting the user to fix it. If everything is okay, the program will start the analysis and display the results in the second tab
called **Valeurs**.

.. image:: _images/values.png
   :width: 300

The **Valeurs** tab shows the discretized data for a specified layer, chosen by the user by selecting it in the **Numéro de la couche à traiter** combobox.
It also shows the Morphological Indicator for the analyzed file. The user can choose to export those indicators in a file by clicking on the **Exporter les variables** button.

.. note::
   The **V_scan** field is actually writable. It is used by the user to enter the volume given by the scanner, which is used to calculate some Morphological Indicators. Once the user
   puts a value in the **V_scan** field, they must click the **Recalculer les indicateurs morphologiques** button to update the values.


Graphs
^^^^^^

If the user chooses to, the program can show some graphs of the analyzed file. The graph panel contains 4 tabs, each with a different layout. The first two tabs are in a 2x2 grid layout, the third has a 2x1 vertical layout, and the last one uses the entire available space. The user can choose which graphs to show in each slot by selecting them in the slots combo box.

The available graphs are the following:

**Mesh 3D**: Show an interactive 3D representation of the scanned object.

.. image:: _images/mesh_3d.png
   :width: 500

The controls are a bit wonky, but there is currently no way to change them:

- To zoom, use the mouse wheel or hold down the right mouse button and move the mouse up or down.
- To move, hold down the **Shift** key, then click on the graph and move the mouse.
- To rotate, hold down the left mouse button and move the mouse:
    - For a rotation around the x-axis, move the mouse up or down in the area indicated in blue.
    - For a rotation around the y-axis, move the mouse in a circular trajectory following the area indicated in green.
    - For a rotation around the z-axis, move the mouse left or right in the area indicated in red.

**Coupe XZ/YZ**: Show a 2D slice of the scanned object.

.. image:: _images/cross_section.png
   :width: 500

The controls are the same for every 2D graph:

- To zoom in and out, use the mouse wheel.
- To move, click on the graph and move the mouse.
- To change the scale, right-click on the graph and move the mouse:
    - Up to increase the scale of the y-axis.
    - Down to decrease the scale of the y-axis.
    - Left to decrease the scale of the x-axis.
    - Right to increase the scale of the x-axis.

**Evolution du rayon moyen**: Show the evolution of the mean radius for each discrete layer of the scanned object.

.. image:: _images/mean_radius_evolution.png
   :width: 500

**Difference entre les rayons moyen et la moyenne des rayons moyens**: Show the difference between the mean radius and the mean of the mean radius for each discrete layer of the scanned object.

.. image:: _images/r_z-r_mean.png
   :width: 500

**Coupe de la couche**: Show a 2D slice of the scanned object for a specified layer.

.. image:: _images/layer_cross_section.png
   :width: 500

**Difference entre le rayon de chaque point et le rayon moyen de la couche**: Show the difference between the radius of each point and the mean radius of the layer for a specified layer.

.. image:: _images/layer_r-r_mean.png
   :width: 500

The controls are the same for every 2D graph.

- To zoom in and out, use the mouse wheel.
- To move, click on the graph and move the mouse.
- To change the scale, right-click on the graph and move the mouse:
    - Up to increase the scale of the y-axis.
    - Down to decrease the scale of the y-axis.
    - Left to decrease the scale of the x-axis.
    - Right to increase the scale of the x-axis.

.. _Settings configuration:

Settings
^^^^^^^^

The settings panel allows the user to change the default settings of the program. The settings are saved in a file called `config.yml` in the same folder as the program. The available settings are as follows:

.. image:: _images/settings.png
   :width: 500

- **Methode de discretisation**: The method used to discretize the scanned object. The available methods are:
    - **Z0-Zi >= DeltaZ**: The thickness of the layer is equal to or slightly larger than DeltaZ.
    - **Z0-Zi < DeltaZ**: The thickness of the layer is strictly smaller than DeltaZ but as close as possible.
- **Mettre l'objet droit**: If checked, the object will be aligned vertically before the analysis.
- **Suffixe des fichiers de données brutes**: The suffix of the raw data files.
- **Suffixe des fichiers de données discrétisées**: The suffix of the discrete data files.
- **extension des fichiers de sorties**: The extension of the output files.
- **Separateur des colones**: The separator used in the output files (use "\t" for tabulation).
- **Aligner les colones**: If checked, the columns will be aligned in the output files.

Scripting API
-------------

If you wish to use the program's functions without using the GUI, you can utilize the `scripting API`_. The documentation can be found on the :doc:`api` page. Here is an example of what running the analysis looks like when done by a script:

.. code-block:: python

   from utils.files.input import ScannedObject
   from utils.files.output import format_data, save_output_file
   from utils.data_processing.data_processing import get_advanced_data, get_raw_data, get_discrete_data
   from utils.math.position_manipulation import verticalise
   from utils.graph2D.visplot_render import *

   # Read the file you want to analyse and store it a scanned object instance
   obj = ScannedObject.from_file("dataset/0 - Essai sans erosion.obj") #must end with .xzy, .obj or .stl
   # Verticalise the object
   verticalise(obj)
   # Normalise the object on the z axis
   obj.normalise('z')
   # Get the raw data with 6 digit after the comma and a discretisation of 1mm
   raw_data = get_raw_data(obj,6,1)
   # Get the discrete data with 6 digit after the comma and a discretisation of 1mm
   discrete_data = get_discrete_data(obj,6,1)
   # Get the morphological indicators from the discrete data
   morpho_indicators = get_advanced_data(discrete_data)

   # Plot the 2D Cross section on the ZX axis
   vertices = obj.get_vertices()
   x_values = [v[0] for v in vertices]
   z_values = [v[2] for v in vertices]
   cross_section(x_values,
                  z_values,
                  "Coupe XZ",
                  "X (mm)",
                  "Z (mm)",
                  True)   

   # format the data for exportation
   formatted_raw_data = format_data(raw_data,";")
   formatted_discrete_data = format_data(discrete_data,";")

   #save the formatted data to a file
   save_output_file("raw_data.txt",formatted_raw_data)
   save_output_file("discrete_data.txt",formatted_discrete_data)

